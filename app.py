import os
import requests
import web
from dotenv import load_dotenv
import json

# Generating routes
urls = (
    '/(.*)', 'weather'
)
app = web.application(urls, globals())

load_dotenv()
API_TOKEN = os.getenv('API_TOKEN')

K = 273.15

class weather:
    def GET(self, city):
        web.header('content-type', 'application/json') # text/xml
        r = requests.get('http://api.openweathermap.org/data/2.5/weather?zip={zip},fr&appid={}'.format(API_TOKEN, zip=city))
        data = r.json()

        result = {
            'max_temp': data['main']['temp_max'] - K,
            'actual_temp': data.get('main').get('temp') - K,
            'min_temp': data['main']['temp_min'] - K,
            'cloud': data['weather'][0]['main'],
        }
        return json.dumps(result)

if __name__ == "__main__":
    app.run()