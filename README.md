# Weather Webservice
## Sujet

Implémentez un web service qui retourne la météo prévisionnelle française à partir d'un code postal.
Ce code postal français doit être communiqué en passation de paramètre d'url.

Ainsi, votre service doit répondre à une route : <domain.tld>/weather?zipcode={zip}

Et retourner uniquement les informations suivantes :

- La température actuelle
- La température min et max de la journée
- La météo

Je me suis baser sur l'api de [openweathermap.org](https://home.openweathermap.org).

